import * as mysql from 'mysql';

class Database {
  public pool;

  constructor(config) {
    this.pool = mysql.createPool(config);
  }

  query(sql, callback) {
    this.pool.getConnection((err, connection) => {
      if (err) throw err;

      connection.query(sql, (err, result, fields) => {
        connection.release();

        if (err) {
          return callback(err);
        }

        callback(null, result, fields);
      });
    });
  }

}

export default Database;
