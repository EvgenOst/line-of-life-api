import * as express from 'express';
import Database from './database/Database';

class App {
  public express;
  private db;

  constructor() {
    this.express = express();
    // this.mountRoutes();
    this.db = new Database({
      connectionLimit: 10,
      host: 'localhost',
      user: 'app',
      password: 'secret',
      database: 'app_db'
    });

    this.db.query('select 1+1 as solution, 5*6 as multi', (err, res, fields) => {
      console.log('Error', err);
      console.log('Result', res);
      console.log('Field', fields);
    });
  }

  private mountRoutes(): void {
    const router = express.Router();
    router.get('/', (req, res) => {
      res.json({
        message: 'Hello World!',
      });
    });
    this.express.use('/', router);
  }
}

export default App;
